from wtforms import Form, StringField, PasswordField, validators

#Definition et déscription des champs du formulaire sur les données bancaires
class infoCardForm(Form):
    CardNumber = StringField('CardNumber', [validators.Regexp(message='Le code de la carte est constitué de 16 chiffres exactement \n', regex=r'^[0-9]{16}$')])
    CardCrypto = StringField('CardCrypto', [validators.Regexp(message='Le code confidentiel est constitué de 3 chiffres exactement \n', regex=r'^[0-9]{3}$')])

#Definition et déscription des champs du formulaire sur les informations utilisateurs
class connexionUser(Form):
	# voir http://emailregex.com/ pour des détails
    EmailUser = StringField('EmailUser', [validators.Regexp(message='Ceci n\'est pas une adresse email valide \n', regex=r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')])

    # voir https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
    PasswordUser = PasswordField('PasswordUser', [validators.Regexp(message='Votre mot de passe doit faire entre 8 et 20 caractères, avec au minimum 1 majuscule, 1 minuscule et 1 chiffre \n', regex=r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$')])
