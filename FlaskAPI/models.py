import datetime
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Float
from app import database

'''
    *getListeDate*
    Fourni un pattern de dates possibles pour les forms
    @return  listDate : liste des dates
'''
def getListDate():
	listDate = [] 
	year = int(datetime.date.today().year)
	for i in range (year,year + 4):
		for j in range (1,13):
			if(j < 10):
				mois = str(0) + str(j)
			else:
				mois = str(j)
			listDate.append(mois + "/" + str(i))
	return listDate

'''
    *Banque*
    =======================
    #id_banque = Integer
    #nom = String
'''
class Banque(database.Model):
    __tablename__ = 'Banque'
    id_banque = Column(Integer, primary_key=True)
    nom = Column(String(50), unique=True)

    def __init__(self, nom):
        self.nom = nom

    def __repr__(self):
        return '<Banque %r>' % (self.nom)

'''
    *Utilisateur*
    =======================
    #id_user = Integer
    #email = String
    #motDePasse = String
    #numeroCarte = String
    #cryptogramme = String
    #dateFinValide = DateTime
'''
class Utilisateur(database.Model):
    __tablename__ = 'Utilisateur'
    id_user = Column(Integer, primary_key=True)
    email = Column(String(120), unique=True)
    motDePasse = Column(String(120), unique=False)
    numeroCarte = Column(String(16), unique=True)
    cryptogramme = Column(String(3), unique=False)
    dateFinValidite= Column(DateTime, unique=False)

    def __init__(self, email, motDePasse, numeroCarte, cryptogramme, dateFinValidite):
        self.email = email
        self.motDePasse = motDePasse
        self.numeroCarte = numeroCarte
        self.cryptogramme = cryptogramme
        self.dateFinValidite = dateFinValidite

    def __repr__(self):
        return '<Utilisateur %r>' % (self.nom)

'''
    *SiteMarchand*
    =======================
    #id_siteMarchand = Integer
    #id_banque = Integer
    #denomination = String
    #identification = String
    #iban = String
'''
class SiteMarchand(database.Model):
    __tablename__ = 'SiteMarchand'
    id_siteMarchand = Column(Integer, primary_key=True)
    id_banque = Column(Integer, ForeignKey('Banque.id_banque'))
    denomination = Column(String(50), unique=True)
    identification = Column(String(20), unique=True)
    iban = Column(String(27), unique=True)

    def __init__(self, id_banque, denomination, identificiation, iban):
        self.id_banque = id_banque
        self.denomination = denomination
        self.identification = identificiation
        self.iban = iban

    def __repr__(self):
        return '<SiteMarchand %r>' % (self.denomination)

'''
    *Transaction*
    =======================
    #id_transaction = Integer
    #id_siteMarchand = Integer
    #dateTransaction = DateTime
    #cout = String
    #titulaire = String
    #status = Integer
'''
class Transaction(database.Model):
    __tablename__ = 'Transaction'
    id_transaction = Column(Integer, primary_key=True)
    id_siteMarchand = Column(Integer, ForeignKey('SiteMarchand.id_siteMarchand'))
    dateTransaction = Column(DateTime, unique=False)
    cout = Column(Float, unique=False)
    titulaire = Column(String(100), unique=False)
    statut = Column(Integer, unique=False)

    def __init__(self, id_siteMarchand, dateTransaction, cout, titulaire, statut):
        self.id_siteMarchand = id_siteMarchand
        self.dateTransaction = dateTransaction
        self.cout = cout
        self.titulaire = titulaire
        self.statut = statut

    def __repr__(self):
        return '<Transaction %r>' % (self.cout  )
