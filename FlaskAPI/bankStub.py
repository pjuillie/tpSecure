from datetime import datetime
import models as m
from app import database

'''
    Cette fonction est un bouchon qui réalise le comportement de validation de la banque 
'''
def validateBankTransaction(DonneeCarte, commande):

    clePersonnelle = commande["paiement"]["cle_personelle"]

    try :
        if m.SiteMarchand.query.filter_by(identification=clePersonnelle).count() == 0:
            return False
        else:
            # Si le site marchand existe
            entreprise = m.SiteMarchand.query.filter_by(identification=clePersonnelle).first()
            if m.Banque.query.filter_by(id_banque=entreprise.id_banque).count() == 0:
                return False
            else:
                # Si la banque du site parchand existe
                banque = m.Banque.query.filter_by(id_banque=entreprise.id_banque).first()

                # On créer une transaction et on l'enregistre en base de données
                transaction = m.Transaction(entreprise.id_siteMarchand, datetime.now(),
                                            commande["paiement"]["total_due"],
                                            entreprise.denomination, 0)
                database.session.add(transaction)
                database.session.commit()
                return True
    except TypeError:
        return False
    except ValueError:
        return False
    except Exception:
        return False
