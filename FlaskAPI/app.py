from flask.ext.api import FlaskAPI
from flask import Flask, request, render_template, redirect, session
from flask.ext.session import Session
from flask_cors import CORS
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from datetime import datetime
from Crypto.Cipher import AES
import hashlib
import os
import test
import forms


app = FlaskAPI(__name__)
app.secret_key = os.urandom(24)
CORS(app)
app.config.from_object(Config)

#Prise en compte de la base de données
database = SQLAlchemy(app)
migrate = Migrate(app, database)

#Variables nécessaires à l'AES
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(s[-1])]

import models
import bankStub

#Index du site (rien ne s'y trouve)
@app.route('/')
def index():
    #Verifie que l'on est passer par la page de resumeCommande
    if 'idclient' in session:
        idclient = session['idclient']
        return 'Logged in as ' + idclient + '<br>'
    return {'Index':'Rien à voir ici'}

#Création de compte utilisateur
@app.route('/AccountCreation/', methods=['GET', 'POST'])
def AccountCreation():
    # Verifie que l'on est passer par la page de resumeCommande
    if 'idclient' in session:
        # Création de formulaire pour les infos utilisateurs et bancaires
        formUser = forms.connexionUser(request.form)
        formInfoCard = forms.infoCardForm(request.form)

        # Dès que l'on a valider les formulaires
        if request.method == 'POST':

            # On récupère la date de validité de la carte
            ChoiceDate = request.form['ChoiceDate']

            # Verifie que les deux formulaires n'ont pas d'erreurs
            testUser = formUser.validate()
            testInfoCard = formInfoCard.validate()

            if (testUser and testInfoCard):

                # Chiffrement du mot de passe de l'utilisateur avec le sha256 pour le stocker chiffré
                encryptMotDePasse = hashlib.sha256((request.form['PasswordUser'] + "5os|gnz@pqaS4$8").encode()).hexdigest()

                # Création AES 128 bits
                obj = AES.new(b'ThisEisFaVkey123', AES.MODE_CBC, b'ThisDisFanVIV456')

                # Chiffrement du numero de carte et du cryptogramme avec l'AES 128 pour les stocker chiffrés
                encryptNumeroCarte = obj.encrypt(pad(request.form['CardNumber']).encode())
                encryptCryptogramme = obj.encrypt(pad(request.form['CardCrypto']).encode())

                # Obtention de la date de validité avec les informations du formulaire
                dateValidite = datetime.strptime(request.form['ChoiceDate'], "%m/%Y")

                # Création de l'utilisateur et sauvegarde en base de données
                u = models.Utilisateur(email=request.form['EmailUser'], motDePasse=encryptMotDePasse,
                                numeroCarte=encryptNumeroCarte, cryptogramme=encryptCryptogramme ,dateFinValidite=dateValidite)
                database.session.add(u)
                database.session.commit()

                #Redirection vers la page d'authentification
                return redirect('/Authenticate/')

            else:
                #Obtention d'une liste de date et redirection
                ListDate = models.getListDate()
                return render_template('creationCompte.html', formUser=formUser, formInfoCard=formInfoCard,
                                       ListDate=ListDate, ChoiceDate=ChoiceDate)
        else:
            # Obtention d'une liste de date et redirection
            ListDate = models.getListDate()
            return render_template('creationCompte.html', formUser=formUser, formInfoCard=formInfoCard, ListDate=ListDate)

# Page d'authentification
@app.route('/Authenticate/', methods=['GET', 'POST'])
def Authenticate():

    # Verifie que l'on est passer par la page de resumeCommande
    if 'idclient' in session:

        # Création du formulaire utilisateur
        form = forms.connexionUser(request.form)

        # Si l'on a validé le formulaire
        if request.method == 'POST':

            # Chiffrement du mot de passe de l'utilisateur avec le sha256 pour vérifié la valeur par rapport à celui stocké
            encryptMotDePasse = hashlib.sha256((request.form['PasswordUser'] + "5os|gnz@pqaS4$8").encode()).hexdigest()

            # Récupération de l'utilisateur suivant le login et mot de passe renseigné
            user = models.Utilisateur.query.filter_by(email=request.form['EmailUser'],
                                                      motDePasse=encryptMotDePasse).first()

            # Si le formulaire est valide et que l'utilisateur existe
            if form.validate() and user:

                # On déchiffre le numéro de carte et le cryptogramme
                obj2 = AES.new(b'ThisEisFaVkey123', AES.MODE_CBC, b'ThisDisFanVIV456')
                decryptNumeroCarte = unpad(obj2.decrypt(user.numeroCarte).decode("utf-8"))
                decryptCryptogramme = unpad(obj2.decrypt(user.cryptogramme).decode("utf-8"))

                # On stocke les informations banquaires dans un tableau
                InfoUser = []
                InfoUser.append(decryptNumeroCarte)
                InfoUser.append(decryptCryptogramme)
                InfoUser.append(user.dateFinValidite)

                # On vérifie que le paiement est valide après avoir envoyé les informations à la banque
                paiementValide = bankStub.validateBankTransaction(InfoUser, session['commande'])

                # Si le paiement est valide, on est redirigé vers la page qui certifie le paiement
                if paiementValide:
                    # Verifier que l'utilisateur existe ou non dans la BDD, si c'est le cas on passe à la page suivante
                    return render_template('paiement.html', Donnee=InfoUser)
                else:
                    return render_template('connexion.html', form=form)

            else:
                return render_template('connexion.html', form=form)

        else:
            return render_template('connexion.html', form = form)

# Page de résumé de la commande
@app.route('/resumeCommande/')
def getData():
    # On récupère le JSON fourni par le site marchand
    commande = test.getJsonExemple()

    # On mets en place la session qui permets de naviger vers les autres pages et on stocke la commande en session
    session['idclient'] = commande["paiement"]["cle_personelle"]
    session['commande'] = commande

    if 'idclient' in session:
        return render_template('resumeCommande.html', commande=commande)

# Page permetant de payer directement
@app.route('/identityBankCard/', methods=['GET', 'POST'])
def DataBankCard():

    # Verifie que l'on est passer par la page de resumeCommande
    if 'idclient' in session:

        # Création du formulaire d'informations bancaires
        form = forms.infoCardForm(request.form)

        # Si l'on a validé le formulaire
        if request.method == 'POST':

            # On récupère la date de validité de la carte
            ChoiceDate = request.form['ChoiceDate']

            # Si le formulaire est valide
            if form.validate():

                # On stocke les informations bancaires dans un tableau
                DonneCard = []
                DonneCard.append(request.form['CardNumber'])
                DonneCard.append(request.form['CardCrypto'])
                DonneCard.append(request.form['ChoiceDate'])

                # On vérifie que le paiement est valide après avoir envoyé les informations à la banque
                paiementValide = bankStub.validateBankTransaction(DonneCard, session['commande'])

                # Si le paiement est valide, on est redirigé vers la page qui certifie le paiement
                if paiementValide:
                    # Verifier que l'utilisateur existe ou non dans la BDD, si c'est le cas on passe à la page suivante
                    return render_template('paiement.html', Donnee = DonneCard)
                else:
                    ListDate = models.getListDate()
                    return render_template('infoCard.html', form=form, ListDate=ListDate, ChoiceDate=ChoiceDate,
                                           montant=session['commande']["paiement"]["total_due"] + " " +
                                                   session['commande']["paiement"]["devise"])

            else:
                ListDate = models.getListDate()
                return render_template('infoCard.html', form = form, ListDate = ListDate, ChoiceDate = ChoiceDate,
                                       montant = session['commande']["paiement"]["total_due"] + " " + session['commande']["paiement"]["devise"])
        else:
            ListDate = models.getListDate()
            return render_template('infoCard.html', form = form, ListDate = ListDate,
                                   montant= session['commande']["paiement"]["total_due"]+" "+session['commande']["paiement"]["devise"])

if __name__=="__main__":
    app.run(debug=True)