import models as m
from app import database
from Crypto.Cipher import AES

#Variables nécessaires à l'AES
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(s[-1])]

# Création des banques
banque1 = m.Banque("Crédit Mutuel")
banque2 = m.Banque("Crédit Agricole")
banque3 = m.Banque("Caisse d'épargne")

# Enregistrement des banques en base de données
database.session.add(banque1)
database.session.add(banque2)
database.session.add(banque3)

# Récupération des banques pour obtenir leurs id
banqueEntreprise1 = m.Banque.query.filter_by(nom="Crédit Mutuel").first()
banqueEntreprise2 = m.Banque.query.filter_by(nom="Crédit Agricole").first()
banqueEntreprise3 = m.Banque.query.filter_by(nom="Caisse d'épargne").first()

# Création AES 128 bits
obj = AES.new(b'ThisEisFaVkey123', AES.MODE_CBC, b'ThisDisFanVIV456')

# Chiffrement des numéros iban avec l'AES 128 pour les stocker chiffrés
encryptIban1 = obj.encrypt(pad("FR7630001007941234567890185").encode())
encryptIban2 = obj.encrypt(pad("FR3330002005500000157841Z25").encode())
encryptIban3 = obj.encrypt(pad("FR3330002005500000157841P25").encode())

# Création des sites marchands
entreprise1 = m.SiteMarchand(banqueEntreprise1.id_banque, "Amazon.fr", "A37jIop562m6Z9pS", encryptIban1)
entreprise2 = m.SiteMarchand(banqueEntreprise2.id_banque, "CDiscount.com", "R56fG2H5lmKp63Z9", encryptIban2)
entreprise3 = m.SiteMarchand(banqueEntreprise3.id_banque, "LDLC.com", "Hy996qlMl6M527er", encryptIban3)

# Enregistrement des sites marchands en base de données
database.session.add(entreprise1)
database.session.add(entreprise2)
database.session.add(entreprise3)

database.session.commit()


