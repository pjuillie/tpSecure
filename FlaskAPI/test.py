import os
import unittest
import tempfile
from flask import request


'''
	Cette fonction permet de fournir un JSON de test pour l'application. 
	
'''
def getJsonExemple():
	commande = {"paiement" : {"redirect_url": "http://127.0.0.1:5000/origin/","cle_personelle": "A37jIop562m6Z9pS","total_due": "150.00",
							  "devise":"euros","details": {"produit1": "62.00","produit2": "38.00","produit3": "52.00"}}}
	return commande

"""
with app.test_request_context('/hello', method='POST'):
    assert request.path == '/hello'
    assert request.method == 'POST'
"""

if __name__ == '__main__':
    unittest.main()

